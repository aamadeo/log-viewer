import React, { Component } from 'react';
import './App.css';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import LogBar from './components/LogBar.js'
import InfoErrorSelector from './components/InfoErrorSelector'
import Logs from './components/Logs.js'
import Filters from './components/Filters.js'
import reducer from './reducers/reducer.js'
import { Col, Row } from 'react-bootstrap'

const store = createStore(reducer)

class App extends Component {
  render() {
    return <Provider store={store}>
        <div>
          <Row>
            <Col xs={12}><LogBar /></Col>
          </Row>
          <Row>
            <Col xs={12}><InfoErrorSelector/></Col>
          </Row>
          <Row>
            <Col xs={7}><Logs/></Col>
            <Col xs={5}>
              <h3 style={{ textAlign: 'right' }}> Filters </h3> 
              <Filters/>
            </Col>
          </Row>
        </div>
    </Provider>
  }
}

export default App;
