import { combineReducers } from 'redux'
import * as actions from '../actions'

const log = ( state = [], { type, payload }) => {
    switch(type){
        case actions.SET_LOG:
            return payload.log
        default:
            return state
    }
}

const initialTabs = {
    active: "void",
    void: "void"
}

const tabs = ( state = initialTabs, { payload, type }) => {
    switch(type){
        case actions.SELECT_LOG:
            return Object.assign({}, state, { active: payload.log })
        case actions.SELECT_LOG_TYPE:
            return Object.assign({}, state, { [payload.log] : payload.type })
        default:
            return state
    }
}

const defaultFilter = object => true
const noFilter = (state = defaultFilter) => state
const byTag = (state = defaultFilter, { type, payload }) => {
    switch(type){
        case actions.UPDATE_TAG_FILTER:
            const { tags } = payload

            return object => tags.reduce( (r,t) => r || t.match(object.message) , false )
        default:
            return state
    }
}

const byAttribute = (state = defaultFilter, { type, payload }) => {
    switch(type){
        case actions.UPDATE_ATTR_FILTER:
            return object => object[payload.attribute] === payload.value
        default:
            return state
    }
}

const getTime = d => new Date(d.date + " " + d.time).getTime()
const logTime = o => new Date(o.timestamp).getTime()
const byDate = (state = defaultFilter, { type, payload }) => {
    switch(type){
        case actions.UPDATE_DATE_FILTER:

            const after = getTime(payload.filter.after)
            const before = getTime(payload.filter.before)

            return object => {
                const time = logTime(object)

                return after < time && time < before
            }
        default:
            return state
    }
}

const custom = (state = defaultFilter, { type, payload }) => {
    switch(type){
        case actions.UPDATE_CUSTOM_FILTER:
            return payload.filter
        default:
            return state
    }
}

const initialActive = { 
    byTag: false, byAttribute: false, custom: false, byDate: false
}
const active = (state = initialActive, { payload, type }) => {
    switch(type){
        case actions.ENABLE_FILTER:
            return Object.assign({}, state, { [payload.filter] : true })
        case actions.DISABLE_FILTER:
            return Object.assign({}, state, { [payload.filter] : false })
        default:
            return state
    }
}

const filter = combineReducers({
    noFilter,
    byTag,
    byAttribute,
    byDate,
    custom,
    active
})

const tags = (state = new Set(), { payload, type }) => {
    switch(type){
        case actions.SET_TAGS:
            return payload.tags
        default:
            return state
    }
}

const mainReducer = combineReducers({
    log,
    tabs,
    tags,
    filter
})

export default (state, action) => {
    const newState = mainReducer(state, action)

    console.log({
        newState,
        action
    })

    return newState
}