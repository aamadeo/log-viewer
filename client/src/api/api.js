const parse = r => r.split("\n").map( s => s && JSON.parse(s) )
const logServer = 'st.aamadeo.me:9999'

export const load = type => {
	const url = `http://${logServer}/${type}.log`

	return fetch(url).then( r => r.text() ).then( t => {
		const logs = parse(t)
		
		const TStoMillis = ts => new Date(ts).getTime()
		const cmp = (a,b) => - TStoMillis(a.timestamp) + TStoMillis(b.timestamp) 
		logs.sort(cmp)
		logs.pop()

        return logs
	}).catch( () => {
        return []
    })
}
