import React from 'react'
import { Panel, PanelGroup, Label, Checkbox, FormControl, InputGroup, Glyphicon, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { updateTagFilter, updateCustomFilter, updateByDateFilter, enableFilter, disableFilter } from '../actions'

const to2d = x => (x < 10 ? "0" : "") + x
const month = d => to2d(d.getMonth() + 1)
const toDate = d => d.getFullYear() + "-" + month(d) + "-" + to2d(d.getDate())
const toTime = d => to2d(d.getHours()) + ":" + to2d(d.getMinutes())
class Filters extends React.Component {
    constructor(props){
        super(props)

        const tags = []

        this.props.tags.forEach( tag => tags.push({ name: tag, checked: true}))

        const beforeDate = new Date()
        const afterDate = new Date(beforeDate.getTime() - 1000 * 60 * 60)

        const after = {
            date: toDate(afterDate),
            time: toTime(afterDate)
        }

        const before = {
            date: toDate(beforeDate),
            time: toTime(beforeDate)
        }

        this.state = {
            after,
            before,
            tags,
            custom: 'return event.a > 50'
        }

        this.props.updateByDateFilter({ after, before })
    }

    toggleFilter(filter){
        const { enable, disable, active } = this.props

        return () => {
            if ( active[filter] ) disable(filter)
            else enable(filter)
        }
    }

    componentWillReceiveProps(props){
        const tags = []

        if ( props.tags !== this.props.tags ) {
            props.tags.forEach( tag => tags.push({ name: tag, checked: true}))

            this.setState({ tags })      
        }
    }

    customChange(e){
        this.setState({ custom: e.target.value })
    }

    updateCustomFilter(){
        const src = 'event => {\n' + this.state.custom + '\n}'
        const filter = object => {
            let result = false

            try {
                return eval(src)(object)
            } catch ( err ){
                console.log(err)
            }

            return result
        }
        
        this.props.updateCustomFilter(filter)
    }

    onDateChange(limit, portion){
        const { updateByDateFilter } = this.props
        const { after, before } = this.state

        return e => {
            const newState = { after, before }
            newState[limit][portion] = e.target.value

            this.setState(newState)

            updateByDateFilter(newState)
        }
    }

    toggleTag(tag){
        const { state } = this
        const { updateTagFilter } = this.props

        return () => {
            const newTags = state.tags.map( t => t.name !== tag ? t : { name: tag, checked: !t.checked })
            const filterTags = newTags.filter( t => t.checked ).map( t => t.name)

            console.log({ filterTags })
            updateTagFilter(filterTags)
            this.setState({ tags: newTags })
        }
    }

    render(){
        const { active } = this.props

        const { after, before } = this.state

        const tags = this.state.tags.map( tag => <li key={tag.name}>
                <Checkbox checked={tag.checked} onChange={this.toggleTag(tag.name)}>
                    <Label>{tag.name}</Label>
                </Checkbox>
            </li>
        )

        const style = { fontFamily: 'monospace' }
        const customChange = this.customChange.bind(this)
        const updateCustomFilter = this.updateCustomFilter.bind(this)
        const onDateChange = this.onDateChange.bind(this)

        const filterStyle = {
            byTag: active.byTag ? 'success' : undefined,
            byAttribute: active.byAttribute ? 'success' : undefined,
            custom: active.custom ? 'success' : undefined,
            byDate: active.byDate ? 'success' : undefined,
        }

        console.log({ active, style })

        return <PanelGroup accordion>
            <Panel bsStyle={filterStyle.byTag} collapsible header='By Tag' eventKey='tag'>
                <Checkbox checked={active.byTag} onChange={this.toggleFilter('byTag')}>
                    Enable filter by tag
                </Checkbox>
                <ul>{tags}</ul>
            </Panel>
            <Panel bsStyle={filterStyle.byAttribute} collapsible header='By Attribute' eventKey='attr'>
                <Checkbox checked={active.byAttribute} onChange={this.toggleFilter('byAttribute')}>
                    Enable filter by attribute
                </Checkbox>
            </Panel>
            <Panel bsStyle={filterStyle.custom} collapsible header='Custom' eventKey='custom'>
                <Checkbox checked={active.custom} onChange={this.toggleFilter('custom')}>
                    Enable custom filter
                </Checkbox>
                <p>Write your own filter: <br/>{'function(event) {' }</p>
                <FormControl componentClass="textarea" placeholder="return event.id === 12345" style={style} onChange={customChange}/>
                {'}'}
                <br />
                <Button onClick={updateCustomFilter}>
                    Update
                </Button>
            </Panel>
            <Panel bsStyle={filterStyle.byDate} collapsible header='ByDate' eventKey='bydate'>
                <Checkbox checked={active.byDate} onChange={this.toggleFilter('byDate')}>
                    Enable ByDate filter
                </Checkbox>
                <InputGroup>
                    <InputGroup.Addon>after</InputGroup.Addon>
                    <FormControl value={after.date} type="date" style={style} onChange={onDateChange("after","date")}/>
                    <FormControl value={after.time} type="time" style={style} onChange={onDateChange("after","time")}/>
                </InputGroup>
                <InputGroup>
                    <InputGroup.Addon>before</InputGroup.Addon>
                    <FormControl value={before.date} type="date" style={style} onChange={onDateChange("before","date")}/>
                    <FormControl value={before.time} type="time" style={style} onChange={onDateChange("before","time")}/>
                </InputGroup>
            </Panel>
        </PanelGroup>
    }
}

const mapStateToProps = ({ tags, filter }) => ({
    tags, 
    active: filter.active
})

const mapDispatchToProps = dispatch => ({
    updateTagFilter: tags => dispatch(updateTagFilter(tags)),
    updateCustomFilter: tags => dispatch(updateCustomFilter(tags)),
    updateByDateFilter: tags => dispatch(updateByDateFilter(tags)),
    enable: filter => dispatch(enableFilter(filter)),
    disable: filter => dispatch(disableFilter(filter))
})

export default connect(mapStateToProps, mapDispatchToProps)(Filters)
