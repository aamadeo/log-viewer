import React from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import { connect } from 'react-redux'
import { selectLogType } from '../actions.js'

class InfoErrorSelector extends React.Component {
    select(key) {
        const { selectLogType, log } = this.props

        return () => selectLogType(log, key)
    }

    render(){
        const { log, active } = this.props

        if ( log === "void" ){
            return <div />
        }

        return <ButtonGroup justified>
            <Button href="#" active={active === 'info'} onClick={this.select('info')} bsStyle="success">
                Info
            </Button>
            <Button href="#" active={active === 'error'} onClick={this.select('error')} bsStyle="danger">
                Error
            </Button>
        </ButtonGroup>
    }
}

const mapStateToProps = ({ tabs }) => ({
    log: tabs.active,
    active: tabs[tabs.active] ? tabs[tabs.active] : 'Info'
})

const mapDispatchToProps = dispatch => ({
    selectLogType: (log,type) => dispatch(selectLogType(log,type))
})

export default connect(mapStateToProps, mapDispatchToProps)(InfoErrorSelector)