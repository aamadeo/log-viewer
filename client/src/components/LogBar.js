import React from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { selectLog } from '../actions.js'
import config from '../config.json'

class LogBar extends React.Component {
    select(key){
        return () => this.props.selectLog(key)
    }

    render(){
        const logs = config.logs
        const { active } = this.props
        const tabs = logs.map( id => 
            <Button href="#" active={id === active} key={id} onClick={this.select(id)} bsStyle="info">
                {id}
            </Button>
        )

        return <ButtonGroup justified >
            {tabs}
        </ButtonGroup>
    }
}

const mapStateToProps = ({ tabs }) => ({
    active: tabs.active
})

const mapDispatchToProps = dispatch => ({
    selectLog: (log) => dispatch(selectLog(log))
})

export default connect(mapStateToProps, mapDispatchToProps)(LogBar)