import React from 'react'
import { connect } from 'react-redux'
import { load } from '../api/api'
import * as actions from '../actions'
import ReactJson from 'react-json-view'
import { Pagination, Glyphicon } from 'react-bootstrap'

class Logs extends React.Component {
    constructor(props){
        super(props)
        this.state = { page : 1 }
    }

    load(name, type){
        const { setLog, setTags, updateTagFilter } = this.props

        name = name || this.props.name
        type = type || this.props.type
        
        const logname = name + (type === 'error' ? '.error' : '')
        const updatePage = page => this.setState({ page })

        load(logname).then( r => {
            setLog(r)
            const tags = new Set()
            r.forEach( line => tags.add(line.message))

            setTags(tags)

            const tagArray = []
            tags.forEach( tag => tagArray.push(tag))
            updateTagFilter(tagArray)

            updatePage(1)
        })
    }

    componentDidMount(){
        //const intervalId = setInterval(() => console.log(this.props.name, this.props.type), 10 * 1000)

        //this.setState({ intervalId })
    }

    componentWillUnmount(){
        //clearInterval(this.intervalId)
    }

    componentWillReceiveProps(newProps){
        const { name, type } = this.props

        if ( newProps.name !== name || type !== newProps.type){
            this.load(newProps.name, newProps.type)
        }
    }

    selectPage(page){
        this.setState({ page })
    }

    render(){
        const { name, type, log, filter } = this.props

        if ( name === "void" ) return <div />

        let filteredLogs = log

        if ( filter.active.byTag ) filteredLogs = filteredLogs.filter(filter.byTag)

        if ( filter.active.byAttribute ) filteredLogs = filteredLogs.filter(filter.byAttribute)

        if ( filter.active.custom ) filteredLogs = filteredLogs.filter(filter.custom)

        if ( filter.active.byDate ) filteredLogs = filteredLogs.filter(filter.byDate)

        const pageSize = 20
        const selectPage = this.selectPage.bind(this)
        const pages = Math.ceil(filteredLogs.length / pageSize)
        const page = this.state.page
        const start = pageSize * (page -1)
        
        const pageFilter = (element, i) => (i < start || i >= (start + pageSize)) ? undefined : element

        filteredLogs = filteredLogs.filter(pageFilter)

        const logContent = filteredLogs.map( (line,i) => {
            const object = Object.assign({}, line)
        
            delete object.timestamp
            delete object.message

            let className=''
            if ( line.message.match("error") && type !== 'error'){
                className='error'
            }

            return <tr key={i} className={className}>
                <td>
                    {new Date(line.timestamp).toLocaleString()}
                </td>
                <td>
                    {line.message}
                </td>
                <td>
                    <ReactJson 
                        src={object} 
                        displayDataTypes={false} 
                        displayObjectSize={false}
                        name="detail" 
                        collapsed={true} />
                </td>
            </tr>
        })

        return <div>
            <h3>
                {name.charAt(0).toUpperCase() + name.substring(1)}.{type}
                <a href="#" onClick={() => this.load()} style={{ margin: "3vw", fontSize: "2.5vw" }}>
                    <Glyphicon glyph="refresh"/>
                </a>
            </h3>
                
            <Pagination
                prev
                next
                first
                last
                ellipsis
                boundaryLinks
                items={pages}
                maxButtons={5}
                activePage={page}
                onSelect={selectPage} />
            <table><tbody>{logContent}</tbody></table>
        </div>
    }
}

const mapStateToProps = ({ tabs, log, filter }) => ({
    name: tabs.active,
    type: tabs[tabs.active] || 'Info',
    log,
    filter,
})

const mapDispatchToProps = dispatch => ({
    setLog: log => dispatch(actions.setLog(log)),
    setTags: tags => dispatch(actions.setTags(tags)),
    updateTagFilter: tags => dispatch(actions.updateTagFilter(tags))
})

export default connect(mapStateToProps, mapDispatchToProps)(Logs)