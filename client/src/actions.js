export const SELECT_LOG = 'SELECT_LOG'
export const selectLog = log => ({
    payload: { log },
    type: SELECT_LOG
})

export const SELECT_LOG_TYPE = 'SELECT_LOG_TYPE'
export const selectLogType = (log,type) => ({
    payload: { log, type },
    type: SELECT_LOG_TYPE
})

export const SET_LOG = 'SET_LOG'
export const setLog = log => ({
    payload: { log },
    type: SET_LOG
})

export const SET_TAGS = 'SET_TAGS'
export const setTags = tags => ({
    payload: { tags },
    type: SET_TAGS
})

export const UPDATE_TAG_FILTER = 'UPDATE_TAG_FILTER'
export const updateTagFilter = tags => ({
    payload: { tags },
    type: UPDATE_TAG_FILTER
})

export const ENABLE_FILTER = 'ENABLE_FILTER'
export const enableFilter = filter => ({
    payload: { filter },
    type: ENABLE_FILTER
})

export const DISABLE_FILTER = 'DISABLE_FILTER'
export const disableFilter = filter => ({
    payload: { filter },
    type: DISABLE_FILTER
})

export const UPDATE_CUSTOM_FILTER = 'UPDATE_CUSTOM_FILTER'
export const updateCustomFilter = filter => ({
    payload: { filter },
    type: UPDATE_CUSTOM_FILTER
})


export const UPDATE_DATE_FILTER = 'UPDATE_DATE_FILTER'
export const updateByDateFilter = filter => ({
    payload: { filter },
    type: UPDATE_DATE_FILTER
})