const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')

const app = express()
const fs = require('fs')
const http = require('http')
const PORT = process.env.PORT || 9999 

const server = http.createServer(app)

const io = require('socket.io')(server)
server.listen(PORT, function () {
	console.log("Listening on " + PORT)
})

app.use(function (req, res, next) {
	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET')
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:9999')
	
	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
	// Set to true if you need the website to include cookies in the requests sent
	// Pass to next layer of middleware
	next()
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: false
}))

console.log("Serving: ", path.join(__dirname, '../log'))
console.log(__dirname)
app.use(express.static(path.join(__dirname, '../log')))
app.use(express.static(path.join(__dirname, '/public')))